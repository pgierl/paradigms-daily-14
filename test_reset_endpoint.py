import unittest
import requests
import json

class TestReset(unittest.TestCase):

    SITE_URL = 'http://student04.cse.nd.edu:51076' # replace with your port id
    print("Testing for server: " + SITE_URL)
    MOVIES_URL = SITE_URL + '/movies/'
    RESET_URL = SITE_URL + '/reset/'

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_put_reset_index(self):
        m = {}
        m['title'] = 'ABC'
        m['genres'] = 'Sci-Fi|Fantasy'
        r = requests.put(self.MOVIES_URL + '95', data = json.dumps(m))
        r = requests.put(self.MOVIES_URL + '96', data = json.dumps(m))

        m = {}
        r = requests.put(self.RESET_URL, data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')
        
        r = requests.get(self.MOVIES_URL + '95')
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['title'], 'Broken Arrow (1996)')
        self.assertEqual(resp['genres'], 'Action|Thriller')

        r = requests.get(self.MOVIES_URL + '96')
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['title'], 'In the Bleak Midwinter (1995)')
        self.assertEqual(resp['genres'], 'Comedy')
        


    def test_put_reset_key(self):
	#TODO write this entire test
        m = {}
        m['title'] = 'ABC'
        m['genres'] = 'Sci-Fi'
        movie_id = 95
        r = requests.put(self.MOVIES_URL + str(movie_id), data = json.dumps(m))

        m = {}
        r = requests.put(self.RESET_URL + str(movie_id), data = json.dumps(m))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        r = requests.get(self.MOVIES_URL + str(movie_id))
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['title'], 'Broken Arrow (1996)')
        self.assertEqual(resp['genres'], 'Action|Thriller')
	
	

if __name__ == "__main__":
    unittest.main()
# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
