var send = document.getElementById('send-button');
send.onmouseup = getFormInfo;

var clear = document.getElementById('clear-button');
clear.onmouseup = clearFormInfo;

function clearFormInfo() {
  console.log('entered clearFormInfo');
  document.getElementById('input-port-number').value = '';
  document.getElementById('radio-get').checked = true;
  document.getElementById('input-key').value = '';
  document.getElementById('text-message-body').value = '';
  document.getElementById('checkbox-use-key').checked = false;
  document.getElementById('checkbox-use-message').checked = false;
  document.getElementById('response-label').innerHTML = '';
  document.getElementById('answer-label').innerHTML = '-';
}

function getFormInfo() {
  console.log('enetered getFormInfo');
  var selindex = document.getElementById('select-server-address').selectedIndex;
  var url_base = document.getElementById('select-server-address').options[selindex].value;

  var port = document.getElementById('input-port-number').value;

  var action = "GET";
  if (document.getElementById('radio-get').checked) {
    action = "GET";
  } else if (document.getElementById('radio-put').checked) {
    action = "PUT";
  } else if (document.getElementById('radio-post').checked) {
    action = "POST";
  } else if (document.getElementById('radio-delete').checked) {
    action = "DELETE";
  }

  var key = null;
  if (document.getElementById('checkbox-use-key').checked) {
    key = document.getElementById('input-key').value;
  }

  var msg = null;
  if (document.getElementById('checkbox-use-message').checked) {
    msg = document.getElementById('text-message-body').value;
  }

  makeRequest(url_base, port, action, key, msg);

}

function makeRequest(url_base, port, action, key, msg) {
  console.log('entered makeRequest');
  var url = url_base + ':' + port + '/movies/';
  if (key != null) {
    url += key;
  }
  var xhr = new XMLHttpRequest();
  xhr.open(action, url, true);

  xhr.onload = function(e) {
    console.log(xhr.responseText);
    updateResponse(xhr.responseText);
  }

  xhr.onerror = function(e) {
    console.log(xhr.statusText)
  }

  xhr.send(msg);
}

function updateResponse(response) {
  console.log('entered updateResponse');

  var label = document.getElementById('response-label');
  label.innerHTML = response;
  var label_answer = document.getElementById('answer-label')
  var resp_json = JSON.parse(response);
  if (resp_json['title']) {
    label_answer.innerHTML = resp_json['title'] + ' belongs to the genres: ' + resp_json['genres'];
  } else if (resp_json['result'] == 'success') {
    label_answer.innerHTML = "- Operation was successful."
  } else {
    label_answer.innerHTML = "- An error has occurred."
  }
}
